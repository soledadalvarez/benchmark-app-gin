"""
Entry point for the api
"""
from benchmark_service import BenchmarkServiceBuilder


APP = BenchmarkServiceBuilder.build()
