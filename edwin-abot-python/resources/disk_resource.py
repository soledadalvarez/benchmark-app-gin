"""
Contains the disk bound resource
"""
import json
from tempfile import NamedTemporaryFile

import falcon


class DiskResource:
    """
    Has a method for handling disk bound get requests
    """
    def on_get(self, req: falcon.Request, resp: falcon.Response):
        """Handles GET requests"""

        with open('support/disk_test.csv', 'r') as the_origin:
            test_str = the_origin.read()

        with NamedTemporaryFile(delete=True) as the_destination:
            bytes_written = the_destination.write(test_str.encode())

        resp.status = falcon.HTTP_OK
        resp.media = json.dumps({'bytes': bytes_written})
