'use strict';

const http = require('http');
const cluster = require('cluster');
const numCPUs = require('os').cpus().length;
const endpoints = require('./endpoints/');

if (cluster.isMaster) {
  for (let i = 0; i < numCPUs; i++) {
    cluster.fork();
  }

  console.log('Let the game begin!');
} else {
  http
  .createServer(endpoints)
  .listen(80);
}