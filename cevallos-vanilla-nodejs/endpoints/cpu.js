'use strict';

const crypto = require('crypto');

const algorithm = 'sha512';
const input = 'Sparkers doing some benchmarking';
const encoding = 'base64';

module.exports = function cpu() {
  var hash = null;
  
  for (var i = 0; i < 256; i++) {
    hash = crypto.createHash(algorithm)
    .update(input)
    .digest(encoding);
  }
  
  return {
    Hashed: hash
  };
}