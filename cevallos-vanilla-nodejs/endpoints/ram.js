'use strict';

const fs = require('fs');

const FILEPATH = 'support/ram_test.txt';
const REGEX = /[aeiou]/gi;

module.exports = async function ram() {
  return new Promise((resolve, reject) => {
    fs.readFile(FILEPATH, 'utf8', (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve({
          n_vowels: data.match(REGEX).length
        });
      }
    });
  });
}