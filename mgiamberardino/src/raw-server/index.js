const Server = require('./server');
const API = require('../api');

new Server()
  .register(API.routes)
  .start();
