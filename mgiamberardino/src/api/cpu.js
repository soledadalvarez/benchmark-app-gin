const crypto = require('crypto');
const data = 'Sparkers doing some benchmarking';

function hash512(data, times){
  times = times || 1;
  for (let i = 0; i < times; i++){
    data = crypto
              .createHash('sha512')
              .update(data)
              .digest('HEX');
  }
  return data;
}

module.exports = function (request) {
  return {
    Hashed: hash512(data, 256)
  };
}
