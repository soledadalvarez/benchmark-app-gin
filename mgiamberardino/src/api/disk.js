const fs = require('fs');
const uuid = require('uuid/v4');
const stream = require('stream');
const Transform = stream.Transform;

class CopyAndCount extends Transform {
  constructor(options){
    super(options);
    this.byteLength = 0;
  }
  _transform(chunk, enc, next){
    this.byteLength += chunk.length;
    this.push(chunk);
    next();
  }
  get bytes(){
    return this.byteLength;
  }
}

const copyCountDelete = (callback) => {
  const cac = new CopyAndCount();
  const newFile = `/tmp/${uuid()}.tmp`;
  fs.createReadStream('/support/disk_test.csv')
    .pipe(cac)
    .pipe(fs.createWriteStream(newFile))
    .on('finish', () => {
      fs.unlinkSync(newFile);
      callback({
        bytes: cac.bytes
      });
    });
};

module.exports = () => {
  return new Promise((resolve, reject) => {
    copyCountDelete(resolve);
  });
};
