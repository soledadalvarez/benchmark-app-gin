package main

import (
	"github.com/gin-gonic/gin"
	"log"
	"io/ioutil"
	"os"
	"net/http"
	"regexp"
	"crypto/sha512"
	"encoding/base64"
)

const THROUGHPUT = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In in ipsum a velit faucibus tempor vel nec odio. Interdum et malesuada fames ac ante ipsum primis in faucibus. Curabitur quis orci eget purus tempus aliquet eu eu risus. Ut velit elit, viverra et ex vel, scelerisque rhoncus odio. Donec vitae diam pellentesque, commodo velit et, lacinia leo. In vel pharetra purus, sed eleifend nunc. Maecenas porta rhoncus consectetur. In hac habitasse platea dictumst. Duis sed erat nibh. Morbi imperdiet lorem purus, vitae facilisis enim maximus et. Phasellus ullamcorper sapien eget neque eleifend malesuada."
const DISK_FILE = "support/disk_test.csv"
const TEMP_FILE_PREFIX = "disk_test_temp"
const RAM_FILE = "support/ram_test.txt"
const CPU_TEXT2HASH = "Sparkers doing some benchmarking"

func main() {
	r := gin.Default()
	r.GET("/throughput", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"throughput": THROUGHPUT,
		})
	})
	r.GET("/disk", func(c *gin.Context) {
		c.JSON(http.StatusOK, handleDisk())
	})
	r.GET("/cpu", func(c *gin.Context) {
		c.JSON(http.StatusOK, handleCPU())
	})
	r.GET("/ram", func(c *gin.Context) {
		c.JSON(http.StatusOK, handleRAM())
	})
	r.Run(":80")
}

// Reads a test file and dumps the content into a temporal file.
func handleDisk() gin.H {
	content, err := ioutil.ReadFile(DISK_FILE)
	if err != nil {
		log.Fatal(err)
	}
	createTempFile(content)
	return gin.H{"bytes": len(content)}
}

// Encrypts a String by using SHA-512.
func handleCPU() gin.H {
	sha512 := sha512.New()
	encodedHash := []byte(CPU_TEXT2HASH)
	for i := 0; i < 256; i++ {
		sha512.Write([]byte(encodedHash))
		encodedHash = sha512.Sum(nil)
	}
	return gin.H{"Hashed": base64.URLEncoding.EncodeToString(encodedHash)}
}

// Reads a file and counts vowels.
func handleRAM() gin.H {
	content, err := ioutil.ReadFile(RAM_FILE)
	if err != nil {
		log.Fatal(err)
	}
	pattern := regexp.MustCompile("[aeiouAEIOU]")
	count := len(pattern.FindAllString(string(content), -1))
	return gin.H{"n_vowels": count}
}

func createTempFile(content []byte) {
	tmpFile, err := ioutil.TempFile(os.TempDir(), TEMP_FILE_PREFIX)
	if err != nil {
		log.Fatal(err)
	}
	defer os.Remove(tmpFile.Name())
	if _, err := tmpFile.Write(content); err != nil {
		log.Fatal(err)
	}
	if err := tmpFile.Close(); err != nil {
		log.Fatal(err)
	}
}