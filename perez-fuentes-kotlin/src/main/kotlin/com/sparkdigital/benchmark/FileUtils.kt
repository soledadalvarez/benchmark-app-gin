package com.sparkdigital.benchmark

import java.io.File
import java.nio.charset.Charset
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.nio.file.StandardOpenOption


object FileUtils {

    fun countVowelsInFile(input: String): Int {
        val content = String(Files.readAllBytes(Paths.get(input)), Charset.forName("UTF-8"))
        return content.replace("[^aeiouAEIOU]", "").length
    }

    fun copyToTempFile(input: String): FileMetadata {
        val inputBytes = Files.readAllBytes(Paths.get(input))
        val tempFile = File.createTempFile("tempfiles", ".tmp")
        return FileMetadata(
                Files.write(Paths.get(tempFile.absolutePath), inputBytes, StandardOpenOption.WRITE),
                inputBytes.size)
    }

}

data class FileMetadata(val path: Path, val size: Int)
