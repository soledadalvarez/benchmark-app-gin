package com.sparkdigital.benchmark

import java.security.MessageDigest


object HashUtils {
    const val HEX_CHARS = "0123456789ABCDEF"

    inline fun sha512(input: String, iterations: Int = 1): String = hashString("SHA-512", input, iterations)

    inline fun hashString(type: String, input: String, iterations: Int): String {
        var inputBytes = input.toByteArray()
        val messageDigest = MessageDigest
                .getInstance(type)

        for (i in 1..iterations) {
            inputBytes = messageDigest
                    .digest(inputBytes)
        }

        return bytesToHex(inputBytes)
    }

    inline fun bytesToHex(inputBytes: ByteArray): String {
        val result = StringBuilder(inputBytes.size * 2)

        inputBytes.forEach {
            val i = it.toInt()
            result.append(HEX_CHARS[i shr 4 and 0x0f])
            result.append(HEX_CHARS[i and 0x0f])
        }
        return result.toString()
    }

}
