package com.sparkdigital.benchmark

import io.vertx.core.AbstractVerticle
import io.vertx.core.Future
import io.vertx.core.Handler
import io.vertx.core.http.HttpServerResponse
import io.vertx.ext.web.Router
import io.vertx.ext.web.RoutingContext
import java.nio.file.Files

@Suppress("unused")
class BenchmarkApiVerticle : AbstractVerticle() {

    /**
     * Verticle intialization.
     */
    override fun start(startFuture: Future<Void>) {
        val router = createRouter()

        vertx.createHttpServer()
                .requestHandler { router.accept(it) }
                .listen(config().getInteger("http.port", 80)) { result ->
                    if (result.succeeded()) {
                        startFuture.complete()
                    } else {
                        startFuture.fail(result.cause())
                    }
                }
    }

    /**
     * Routing
     */
    private fun createRouter() = Router.router(vertx).apply {
        get("/throughput").handler(throughputHandler)
        get("/cpu").handler(cpuHandler)
        get("/ram").handler(ramHandler)
        get("/disk").handler(diskHandler)
    }

    /**
     * Handlers
     */
    private val throughputPayload = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In in ipsum a velit faucibus tempor vel nec odio. Interdum et malesuada fames ac ante ipsum primis in faucibus. Curabitur quis orci eget purus tempus aliquet eu eu risus. Ut velit elit, viverra et ex vel, scelerisque rhoncus odio. Donec vitae diam pellentesque, commodo velit et, lacinia leo. In vel pharetra purus, sed eleifend nunc. Maecenas porta rhoncus consectetur. In hac habitasse platea dictumst. Duis sed erat nibh. Morbi imperdiet lorem purus, vitae facilisis enim maximus et. Phasellus ullamcorper sapien eget neque eleifend malesuada."

    private val throughputHandler = Handler<RoutingContext> { req ->
        req.response().asJson("""{"throughput": "$throughputPayload"}""")
    }

    private val cpuHandler = Handler<RoutingContext> { req ->
        val result = HashUtils.sha512("Sparkers doing some benchmarking", 256)
        req.response().asJson("""{"throughput": "$result"}""")
    }

    private val ramHandler = Handler<RoutingContext> { req ->
        val count = FileUtils.countVowelsInFile("support/ram_test.txt")
        req.response().asJson("""{"n_vowels": "$count"}""")
    }

    private val diskHandler = Handler<RoutingContext> { req ->
        val newFile = FileUtils.copyToTempFile("support/disk_test.csv")
        Files.delete(newFile.path)
        req.response().asJson("""{"bytes": "${newFile.size}"}""")
    }

    /**
     * Extension to the HTTP response to output JSON objects.
     */
    fun HttpServerResponse.asJson(obj: String) {
        this.putHeader("Content-Type", "application/json; charset=utf-8").end(obj)
    }

}
