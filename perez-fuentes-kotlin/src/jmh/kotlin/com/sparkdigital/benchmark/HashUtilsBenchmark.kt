package com.sparkdigital.benchmark

import org.openjdk.jmh.annotations.*

@State(Scope.Benchmark)
@Fork(3)
@Warmup(iterations = 5)
@Measurement(iterations = 5)
open class HashUtilsBenchmark {

    private val value = "Sparkers doing some benchmarking"

    @Setup
    fun setUp(): Unit {
    }

    @Benchmark
    fun cpu256IterationsBenchmark(): String = HashUtils.sha512(value, 256)

    @Benchmark
    fun cpuSingleIterationBenchmark(): String = HashUtils.sha512(value, 1)

}
