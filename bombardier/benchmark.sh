#! /bin/bash
COUNT=0
HOST=$APP_ADDRESS:$APP_PORT
while ! curl --connect-timeout 1 http://$HOST/throughput; do
    sleep 1
    COUNT=$((COUNT+1))
    if [ $COUNT -eq 15 ]; then exit 1; fi
done

$BOMBARDIER --connections=10 --requests=100000 --timeout=10s --latencies http://$HOST/throughput
sleep 3
$BOMBARDIER --connections=100 --requests=100000 --timeout=10s --latencies http://$HOST/throughput
sleep 6
$BOMBARDIER --connections=1000 --requests=100000 --timeout=10s --latencies http://$HOST/throughput
sleep 9

$BOMBARDIER --connections=10 --requests=100000 --timeout=10s --latencies http://$HOST/ram
sleep 3
$BOMBARDIER --connections=100 --requests=100000 --timeout=10s --latencies http://$HOST/ram
sleep 6
$BOMBARDIER --connections=1000 --requests=100000 --timeout=10s --latencies http://$HOST/ram
sleep 9

$BOMBARDIER --connections=10 --requests=100000 --timeout=10s --latencies http://$HOST/cpu
sleep 3
$BOMBARDIER --connections=100 --requests=100000 --timeout=10s --latencies http://$HOST/cpu
sleep 6
$BOMBARDIER --connections=1000 --requests=100000 --timeout=10s --latencies http://$HOST/cpu
sleep 9

$BOMBARDIER --connections=10 --requests=100000 --timeout=10s --latencies http://$HOST/disk
sleep 3
$BOMBARDIER --connections=100 --requests=100000 --timeout=10s --latencies http://$HOST/disk
sleep 6
$BOMBARDIER --connections=1000 --requests=100000 --timeout=10s --latencies http://$HOST/disk
