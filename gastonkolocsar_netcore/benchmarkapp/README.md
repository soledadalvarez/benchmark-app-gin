### Docker 

Build the image running
```
docker build . -t benchmark-app
```

And then run it (use 80 instead of 5050)
```
docker run -p 5050:80 benchmark-app
```