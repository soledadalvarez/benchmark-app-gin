﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using System.Text.RegularExpressions;

namespace Benchmark_WS.Controllers
{    
    public class BenchmarkController : Controller
    {        
        [HttpGet]
        [Route("throughput")]
        public async Task<IActionResult> Echo()
        {                       
            return Ok("Lorem ipsum dolor sit amet, consectetur adipiscing elit. In in ipsum a velit faucibus tempor vel nec odio. Interdum et malesuada fames ac ante ipsum primis in faucibus. Curabitur quis orci eget purus tempus aliquet eu eu risus. Ut velit elit, viverra et ex vel, scelerisque rhoncus odio. Donec vitae diam pellentesque, commodo velit et, lacinia leo. In vel pharetra purus, sed eleifend nunc. Maecenas porta rhoncus consectetur. In hac habitasse platea dictumst. Duis sed erat nibh. Morbi imperdiet lorem purus, vitae facilisis enim maximus et. Phasellus ullamcorper sapien eget neque eleifend malesuada.");
        }        

        private static string ComputeHash(string input)
        {
            //Sparkers doing some benchmarking
            Byte[] inputBytes = Encoding.UTF8.GetBytes(input);                        

            using (var sha256 = SHA512.Create())
            {
                Byte[] hashedBytes = sha256.ComputeHash(inputBytes);
                return BitConverter.ToString(hashedBytes).Replace("-", "").ToLower();
            }
        }

        private const int iter = 256;
        
        [HttpGet]
        [Route("cpu")]
        public async Task<IActionResult> Hash()
        {
            string value = "Sparkers doing some benchmarking";

            var tasks = new Task<string>[iter];            

            for (int i = 0; i < iter; i++)
            {
                tasks[i] = Task.Run(() => ComputeHash(value));
            };

            await Task.WhenAll(tasks);

            return Ok(new { hash = tasks[0].Result });
        }
        
        [HttpGet]
        [Route("ram")]
        public async Task<IActionResult> Ram()
        {
            var text = await System.IO.File.ReadAllTextAsync(Path.Combine("files", "ram_test.txt"));

            int total = Regex.Matches(text, @"[AEIOUaeiou]").Count;            

            return Ok(new { n_vowels = total});
        }

        [HttpGet]
        [Route("disk")]
        public async Task<IActionResult> Disk()
        {            
            var filePath = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());

            var stream = System.IO.File.Open(Path.Combine("files", "ram_test.txt"), FileMode.Open);
            var length = stream.Length;

            using (var writeStream = new FileStream(filePath, FileMode.Create))
            {
                await stream.CopyToAsync(writeStream);
            }

            return Ok(length);
        }
    }
}
