package com.sparkdigital.benchmarking.api.service;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Service that encrypts a String by using SHA-512
 * 
 * @author Soledad
 *
 */
public class CPUService implements BenchmarkService {

	private static final String CPU = "Sparkers doing some benchmarking";

	private MessageDigest digest;

	/**
	 * Returns a Json with the result of encryption
	 */
	@Override
	public String process() {
		byte[] encodedhash = digest.digest(CPU.getBytes(StandardCharsets.UTF_8));
		for (int i = 1; i < 256; i++) {
			encodedhash = digest.digest(encodedhash);
		}
		digest.reset();
		return "{\"Hashed\": \"" + bytesToHex(encodedhash) + "\"}";
	}

	private String bytesToHex(byte[] hash) {
		StringBuffer hexString = new StringBuffer();
		for (int i = 0; i < hash.length; i++) {
			String hex = Integer.toHexString(0xff & hash[i]);
			if (hex.length() == 1)
				hexString.append('0');
			hexString.append(hex);
		}
		return hexString.toString();
	}

	public CPUService() {
		try {
			digest = MessageDigest.getInstance("SHA-512");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
	}

}
