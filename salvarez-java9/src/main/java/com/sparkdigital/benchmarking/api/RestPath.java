package com.sparkdigital.benchmarking.api;

public interface RestPath {

	public static final String DISK_PATH = "/disk";
	public static final String RAM_PATH = "/ram";
	public static final String CPU_PATH = "/cpu";
	public static final String THROUGHPUT_PATH = "/throughput";
}