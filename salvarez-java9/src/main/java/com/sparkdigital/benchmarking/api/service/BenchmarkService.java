package com.sparkdigital.benchmarking.api.service;

public interface BenchmarkService {

	/**
	 * 
	 * @return a json with the result of the process
	 */
	String process() throws Exception;

}
