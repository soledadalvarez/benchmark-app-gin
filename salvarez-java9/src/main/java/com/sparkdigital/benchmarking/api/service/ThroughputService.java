package com.sparkdigital.benchmarking.api.service;

/**
 * Service that returns a fixed String value
 * 
 * @author Soledad
 *
 */
public class ThroughputService implements BenchmarkService {

	private static final String THROUGHPUT = "{\"throughput\": \"Lorem ipsum dolor sit amet, consectetur adipiscing elit. In in ipsum a velit faucibus tempor vel nec odio. Interdum et malesuada fames ac ante ipsum primis in faucibus. Curabitur quis orci eget purus tempus aliquet eu eu risus. Ut velit elit, viverra et ex vel, scelerisque rhoncus odio. Donec vitae diam pellentesque, commodo velit et, lacinia leo. In vel pharetra purus, sed eleifend nunc. Maecenas porta rhoncus consectetur. In hac habitasse platea dictumst. Duis sed erat nibh. Morbi imperdiet lorem purus, vitae facilisis enim maximus et. Phasellus ullamcorper sapien eget neque eleifend malesuada.\"}";

	/**
	 * Returns a Json with a fixed String value
	 */
	@Override
	public String process() {
		return THROUGHPUT;
	}
}
