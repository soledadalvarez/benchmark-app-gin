module benchmarking.api {
	requires spark.core;
	requires junit;

	exports com.sparkdigital.benchmarking.api;
}
