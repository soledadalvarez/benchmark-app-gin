# Services Benchmark

## JAVA 9

The implementation uses the spark java framework and runs on port 80. 
There is no docker Alpine image for Java 9 at the implementation date.

### Compile 
Compile by running 

```
mvn clean install
```
Note: set the proper java9 path in the maven toolchains configuration

	<toolchain>
    	<type>jdk</type>
    	<provides>
      		<version>1.9</version>
	  		<vendor>oracle</vendor>
    	</provides>
    	<configuration>
      		<jdkHome>C:\Program Files\Java\jdk-9.0.1</jdkHome>
    	</configuration>
  	</toolchain>


### To run the app in dev environment 
```
mvn exec:exec
```

Note: set the proper java9 path in the exec-maven-plugin configuration.

### Docker
Build the image running

```
docker build . -t benchmark-app-java
```

And then run it 

```
docker run -p 80:80 benchmark-app-java
