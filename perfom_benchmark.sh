#! /bin/bash
sed -i  "s/put_the_ip_here/${HOST_IP}/g" ./bombardier/Dockerfile
echo 'docker build -t bombardier ./bombardier' 
docker build -t bombardier ./bombardier
echo 'docker build -t service_to_benchmark ./$COMPETITOR'
docker build -t service_to_benchmark ./$COMPETITOR
git checkout .
echo 'docker run -d -p 8080:80 --rm --name app service_to_benchmark'
docker run -d -p 8080:80 --rm --name app service_to_benchmark 
echo 'docker run --link app:app_env --rm -t --name bombing_jack bombardier 2>&1 | tee -a result_$COMPETITOR.txt'
docker run --rm -t --name bombing_jack bombardier 2>&1 | tee -a result_$COMPETITOR.txt  
echo 'docker stop app'
docker stop app
